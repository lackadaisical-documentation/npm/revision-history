'use strict'

import git, { CallbackFsClient, PromiseFsClient, Errors, ReadTagResult, ReadCommitResult } from 'isomorphic-git'
import fs from 'fs'
import path from 'path'

export type GitTagDetails = {
  [x: string]: {
    message: string
    timestamp: number
    offset: number
  }
}

type TagData = {
  dir: string
  fs: CallbackFsClient | PromiseFsClient
  oid: string
  tag: string
}

/**
 * This function returns an object containing info about git references.
 *
 * @param  {string} directory   The path to the root of the git repo.
 * @param  {Array}  gitTags     A list of Git tags to parse.
 * @returns {Object}             An object containing the tags and resolved commit references.
 */
export async function getGitTagRefs(directory: string, gitTags: string[]): Promise<Record<string, string>> {
  const refs: Record<string, string> = {}
  for (const tag of gitTags) {
    refs[tag] = await git.resolveRef({
      dir: path.resolve(directory),
      fs,
      ref: tag,
    })
  }

  return refs
}

/**
 * This Function gets the data from both annotated and lightweight tags.
 * In the case of a lightweight tag, we grab details from the commit it points to instead.
 *
 * @param {TagData} tagDetails  Details of the tag that we need info on
 * @returns {Promise<ReadTagResult | ReadCommitResult>} Details of the tag or the commit it points to
 */
export async function getTagData(tagDetails: TagData): Promise<ReadTagResult | ReadCommitResult> {
  // We want to get a mesage, time, and offset, so first we check if this is an annotated tag;
  // an error will be thrown that it's a commit if it's a lightweight tag so we grab the commit details instead.
  let tagData: ReadTagResult | ReadCommitResult
  try {
    tagData = await git.readTag(tagDetails)
  } catch (err) {
    if (err instanceof Errors.ObjectTypeError && err.data.actual === 'commit') {
      tagData = await git.readCommit(tagDetails)
    } else {
      throw err
    }
  }
  return tagData
}

/**
 * This function returns information about specified tags from a Git repository.
 *
 * @param  {string}   directory   The path to the root of the git repo.
 * @param  {Array}    gitTags     A list of Git tags to parse.
 * @returns {GitTagDetails}       An object containing information from the tag.
 */
export async function getGitTagDetails(directory: string, gitTags: string[]): Promise<GitTagDetails> {
  const tagDetails: GitTagDetails = {}

  const gitTagRefs = await getGitTagRefs(directory, gitTags)

  for (const [tag, tagRef] of Object.entries(gitTagRefs)) {
    const t: TagData = {
      tag: tag,
      dir: directory,
      fs: fs,
      oid: tagRef,
    }
    const tagData = await getTagData(t)

    if ('tag' in tagData) {
      tagDetails[tag] = {
        message: tagData.tag.message,
        timestamp: tagData.tag.tagger.timestamp,
        offset: tagData.tag.tagger.timezoneOffset,
      }
    } else {
      tagDetails[tag] = {
        message: tagData.commit.message,
        timestamp: tagData.commit.committer.timestamp,
        offset: tagData.commit.committer.timezoneOffset,
      }
    }
  }
  return tagDetails
}
