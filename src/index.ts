'use strict'

import { DateTime } from 'luxon'
import { EOL } from 'os'
import { findRoot, listTags } from 'isomorphic-git'
import { getGitTagDetails, GitTagDetails } from './git/git-utils'
import { orderBy } from 'natural-orderby'
import fs from 'fs'
import makeTable, { TableContent, Row } from '@lackadaisical/table-generator'
import path from 'path'
import semver from 'semver'

export type Approvers = {
  [x: string]: {
    fromDate: string
    position: string
    toDate: string
  }
}

export type RevisionHistoryOptions = {
  approvers?: Approvers
  caption?: string
  'message-from-tags'?: boolean
  title?: string
}

type TableOptions = {
  approvers?: Approvers
  caption?: string
  message?: boolean
}

/**
 * This function determines who should be listed as the approver.
 *
 * @param  {number}   date        The date that the version was approved.
 * @param  {object}   approvers   An object containing approvers.
 * @returns {string}              The name of the approver on this date.
 */
function getApprover(date: number, approvers: Approvers): string | undefined {
  let approverDetails: string | undefined = undefined
  for (const approver in approvers) {
    const values = approvers[approver]
    const fromDate: number = DateTime.fromISO(values.fromDate).toSeconds()
    const name = approver
    const position: string = values.position
    const toDate: number = DateTime.fromISO(values.toDate).toSeconds()
    if (fromDate < date && date < toDate) {
      approverDetails = `${name}${EOL}${position}`
    }
  }
  return approverDetails
}

/**
 * This function returns table row objects from git tags.
 *
 * @param     {GitTagDetails}       tagDetails  Information about the tags in question.
 * @param     {TableOptions} options     An object containing options for table content
 * @returns   {Array}                           An array of table row objects.
 */
function generateTableContent(tagDetails: GitTagDetails, options?: TableOptions): Row[] {
  const tableRows: Row[] = []
  for (const tag in tagDetails) {
    const values = tagDetails[tag]
    // TODO: This should use the timeoffset value.
    const date = DateTime.fromSeconds(values.timestamp).toLocaleString() // This is supposed to default to the system locale
    let row: Row = []
    if (typeof options?.message !== 'undefined' && options.message === true) {
      const message = tagDetails[tag].message
      if (typeof options.approvers !== 'undefined') {
        row = [date, tag, message, getApprover(values.timestamp, options.approvers), '', '']
      } else {
        row = [date, tag, message]
      }
    } else {
      if (typeof options?.approvers !== 'undefined') {
        row = [date, tag, getApprover(values.timestamp, options.approvers), '', '']
      } else {
        row = [date, tag]
      }
    }
    tableRows.push(row)
  }

  return tableRows
}

/**
 * This function returns the contents of a Markdown Document Revision History page.
 *
 * @param   {Array}         tableRows     The content of our table.
 * @param   {TableOptions}  tableOptions  Table options (message, approvers)
 * @returns {string}                      A String containing the table (to be concatenated with our document content)
 */
function generateRevisionHistoryTable(tableRows: Row[], tableOptions: TableOptions): string {
  const columns = {}
  if (typeof tableOptions.message !== 'undefined' && tableOptions.message === true) {
    if (typeof tableOptions.approvers !== 'undefined') {
      Object.assign(columns, {
        '**Revised**': {},
        '**Version**': { alignment: 'centre' },
        '**Description**': {},
        '**Approver**': { alignment: 'centre' },
        '**Signature**': {},
        '**Approved**': { alignment: 'centre' },
      })
    } else {
      Object.assign(columns, {
        '**Revised**': {},
        '**Version**': { alignment: 'centre' },
        '**Description**': {},
      })
    }
  } else {
    if (typeof tableOptions.approvers !== 'undefined') {
      Object.assign(columns, {
        '**Revised**': {},
        '**Version**': { alignment: 'centre' },
        '**Approver**': { alignment: 'centre' },
        '**Signature**': {},
        '**Approved**': { alignment: 'centre' },
      })
    } else {
      Object.assign(columns, {
        '**Revised**': {},
        '**Version**': { alignment: 'centre' },
      })
    }
  }

  const content: TableContent = {
    caption: tableOptions.caption,
    columns: columns,
    rows: tableRows,
    type: 'grid',
  }

  return makeTable(content)
}

/**
 * This function returns the Markdown content for the document revision history page.
 *
 * @param   {string}                    inputDir        The input directory.
 * @param   {RevisionHistoryOptions}    inputOptions    An object containing options concerning table generation.
 * @returns {string}                                    A String containing the document revision history page.
 */
export default async function generateRevisionHistory(
  inputDir: string,
  inputOptions?: RevisionHistoryOptions
): Promise<string> {
  const revisionPage = []

  const gitRootDir = await findRoot({
    filepath: path.resolve(inputDir),
    fs,
  })
  const gitTags = await listTags({ dir: gitRootDir, fs })
  // We only want to include valid semver tags on this page
  const sortedGitTags = orderBy(gitTags).filter((tag) => {
    return semver.valid(tag)
  })

  const options: RevisionHistoryOptions = {}
  if (typeof inputOptions !== 'undefined') {
    options.approvers = typeof inputOptions.approvers === 'undefined' ? undefined : inputOptions.approvers
    options.caption = typeof inputOptions.caption === 'undefined' ? undefined : inputOptions.caption
    options['message-from-tags'] =
      typeof inputOptions['message-from-tags'] === 'undefined' ? false : inputOptions['message-from-tags']
    options.title =
      typeof inputOptions.title === 'undefined'
        ? '# Document Revision History {.unnumbered .unlisted}'
        : inputOptions.title
  } else {
    options.approvers = undefined
    options.caption = undefined
    options['message-from-tags'] = false
    options.title = '# Document Revision History {.unnumbered .unlisted}'
  }

  revisionPage.push(`${options.title}`)
  // Generate our table
  const gitTagDetails = await getGitTagDetails(gitRootDir, sortedGitTags)
  if (typeof options.approvers === 'undefined') {
    const tableOptions = {
      caption: options.caption,
      message: options['message-from-tags'],
    }
    const tableContent = generateTableContent(gitTagDetails, tableOptions)
    revisionPage.push(generateRevisionHistoryTable(tableContent, tableOptions))
  } else {
    const tableOptions = {
      approvers: options.approvers,
      caption: options.caption,
      message: options['message-from-tags'],
    }
    const tableContent = generateTableContent(gitTagDetails, tableOptions)
    revisionPage.push(generateRevisionHistoryTable(tableContent, tableOptions))
  }

  return revisionPage.join(`${EOL}${EOL}`)
}
