# @lackadaisical/revision-history

A module for document revision history tables from git tags.

## Installation and Usage

npm:

`npm install @lackadaisical/revision-history`

Yarn:

`yarn add @lackadaisical/revision-history`

### Usage

`getRevisionHistory (inputDir, options)`

Where:

- `inputDir` is the path that you want to generate a revision history for. It may be anywhere within a git repository.
- `options` :
  ```typescript
  options = {
    approvers?: Approvers
    caption?: string
    'message-from-tags'?: boolean
    title?: string
  }
  ```
  + `approvers` is an object containing approvers:
    ```yaml
    approvers:
    Matt Jolly:
      position: Creator
      fromDate: 1990-01-01
      toDate: 2077-01-01
    ```
  + `caption` is a caption for the output table
  + `message-from-tags` is a boolean value that determines whether or not we want a tag message (or commit message from a lightweight tag)
  + `title` is a markdown string for the page heading
