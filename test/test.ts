'use strict'

import { Extract } from 'unzipper'
import { tmpdir } from 'os'
import fs from 'fs'
import getRevisionHistory, { RevisionHistoryOptions } from '../src/index'
import path from 'path'
import test, { Macro } from 'ava'

process.chdir('./test')

const approvers = {
  'Matt Jolly': {
    position: 'Creator',
    fromDate: '1990-01-01',
    toDate: '2077-01-01',
  },
}
// eslint-disable-next-line @typescript-eslint/no-unused-vars
test.before(async (t) => {
  const outPath = path.resolve(tmpdir(), './test-data/extracted/')
  fs.mkdirSync(outPath, { recursive: true })
  await new Promise<void>((resolve, reject) => {
    fs.createReadStream(path.resolve('./test-data/lightweight.zip'))
      .pipe(Extract({ path: outPath }))
      .on('close', () => resolve())
      .on('error', (error) => reject(error))
  })
  await new Promise<void>((resolve, reject) => {
    fs.createReadStream(path.resolve('./test-data/annotated.zip'))
      .pipe(Extract({ path: outPath }))
      .on('close', () => resolve())
      .on('error', (error) => reject(error))
  })
})

const testMacroLightweight: Macro<[RevisionHistoryOptions | undefined, string]> = async (t, options, expected) => {
  const inputpath = path.resolve(tmpdir(), './test-data/extracted/lightweight')
  const output = fs.readFileSync(path.resolve(expected), 'utf8')
  if (typeof options !== 'undefined') {
    t.is(await getRevisionHistory(inputpath, options), output)
  } else {
    t.is(await getRevisionHistory(inputpath), output)
  }
}
testMacroLightweight.title = (
  providedTitle = 'Test lightweight tag page generation for options:',
  options: RevisionHistoryOptions | undefined
) => `${providedTitle} '${JSON.stringify(options)}'`

test(testMacroLightweight, undefined, './test-output/lightweight.md')
test(testMacroLightweight, { approvers: approvers }, './test-output/lightweight-approvers.md')
test(
  testMacroLightweight,
  { title: '# Document Version History {.unnumbered .unlisted}' },
  './test-output/lightweight-title.md'
)
test(testMacroLightweight, { caption: 'Revision History Table' }, './test-output/lightweight-caption.md')
test(testMacroLightweight, { 'message-from-tags': true }, './test-output/lightweight-message.md')
test(
  testMacroLightweight,
  {
    approvers: approvers,
    caption: 'Revision History Table',
    'message-from-tags': true,
    title: '# Document Version History {.unnumbered .unlisted}',
  },
  './test-output/lightweight-all.md'
)

const testMacroAnnotated: Macro<[RevisionHistoryOptions | undefined, string]> = async (t, options, expected) => {
  const inputpath = path.resolve(tmpdir(), './test-data/extracted/annotated')
  const output = fs.readFileSync(path.resolve(expected), 'utf8')
  if (typeof options !== 'undefined') {
    t.is(await getRevisionHistory(inputpath, options), output)
  } else {
    t.is(await getRevisionHistory(inputpath), output)
  }
}
testMacroAnnotated.title = (
  providedTitle = 'Test Annotated tag page generation for options:',
  options: RevisionHistoryOptions | undefined
) => `${providedTitle} '${JSON.stringify(options)}'`

test(testMacroAnnotated, undefined, './test-output/annotated.md')
test(testMacroAnnotated, { approvers: approvers }, './test-output/annotated-approvers.md')
test(
  testMacroAnnotated,
  { title: '# Document Version History {.unnumbered .unlisted}' },
  './test-output/annotated-title.md'
)
test(testMacroAnnotated, { caption: 'Revision History Table' }, './test-output/annotated-caption.md')
test(testMacroAnnotated, { 'message-from-tags': true }, './test-output/annotated-message.md')
test(
  testMacroAnnotated,
  {
    approvers: approvers,
    caption: 'Revision History Table',
    'message-from-tags': true,
    title: '# Document Version History {.unnumbered .unlisted}',
  },
  './test-output/annotated-all.md'
)
// eslint-disable-next-line @typescript-eslint/no-unused-vars
test.after.always((t) => {
  fs.rm(path.resolve(tmpdir(), './test-data'), { recursive: true, force: true }, (err) => {
    if (err) {
      throw err
    }
  })
})
